# proprietary_vendor_motorola_firmware

Stock firmware images for a bunch of motorola devices, to include custom ROM builds.

### Supported devices
* motorola edge 30 (dubai)

### How to use?

1. Clone this repo to `vendor/motorola/firmware`

2. Inherit the appropriate firmware from `device.mk`, for example:

```
# Firmware
$(call inherit-product-if-exists, vendor/motorola/firmware/dubai/config.mk)
```
